import 'package:flutter/material.dart';
import 'package:menu_task/general/constants/MyColors.dart';
import 'package:menu_task/general/constants/MyText.dart';

Widget location(
    {String title,
    String companyName,
    String companyAddress,
    String companyReception,
    String name,
    String phone,String img,BuildContext context}) {
  return Container(
    padding: const EdgeInsets.all(10),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      border: Border.all(color: MyColors.grey.withOpacity(.3),width: 2),
    ),
    child: Column(
      children: [
        MyText(title: companyName,size: 18,color: Colors.black,),
        SizedBox(height: 10,),
        Image.network(img,width: MediaQuery.of(context).size.width,height: 80,),
        SizedBox(height: 10,),
        ListTile(
          leading: Icon(Icons.segment,color: MyColors.grey.withOpacity(.3),),
          title: MyText(
            title:"Company Address",
            color: Colors.black,
            size: 14,
            overflow: TextOverflow.ellipsis,
          ),
          subtitle: MyText(
            title: companyAddress,
            color: MyColors.grey,
            size: 12,
          ),
        ),
        ListTile(
          leading: Icon(Icons.person,color: MyColors.grey.withOpacity(.3),),
          title: MyText(
            title:"Company Reception",
            color: Colors.black,
            size: 14,
            overflow: TextOverflow.ellipsis,
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                title: name,
                color: MyColors.grey,
                size: 12,
              ), MyText(
                title: phone,
                color: MyColors.grey,
                size: 12,
              ),
            ],
          ),
        )
      ],
    ),
  );
}
