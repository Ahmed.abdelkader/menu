import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menu_task/app/screens/MenuPage.dart';
import 'package:menu_task/general/constants/LabelTextField.dart';
import 'package:menu_task/general/constants/MyColors.dart';
import 'package:menu_task/general/constants/MyText.dart';
import 'package:menu_task/app/models/locationDetailsModel.dart';
import 'package:menu_task/app/models/locationsModel.dart';
import 'package:menu_task/app/repository/Repository.dart';
import 'package:menu_task/app/widgets/location.dart';

class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  TextEditingController _searchWork = TextEditingController();
  Repository _repository;
  TabController _controller;
  LocationsModel _locationsModel = LocationsModel();
  LocationDetailsModel _detailsModel = LocationDetailsModel();
  bool _loading = false;
  bool _details = false;
  bool _loadingPage = false;

  @override
  void initState() {
    setState(() {
      _repository = Repository(_scaffold);
    });
    _controller = TabController(length: 3, vsync: this);

    super.initState();
  }

  _getLocations() async {
    setState(() {
      _loading = true;
    });
    _locationsModel = await _repository.getLocations(_searchWork.text);
    setState(() {
      _loading = false;
    });
  }

  _getLocationDetails(String id) async {
    setState(() {
      _loadingPage=true;
    });
    _detailsModel = await _repository.getLocationDetails(id);
    setState(() {
      _details = true;
      _loadingPage = false;
    });
  }

  Widget viewDetails() {
    return Column(
      children: [
        location(
            title: _detailsModel.result.name,
            context: context,
            img: _detailsModel.result.icon,
            name: _detailsModel.result.name,
            phone: _detailsModel.result.formattedPhoneNumber ?? "",
            companyAddress: _detailsModel.result.formattedAddress,
            companyName: _detailsModel.result.name,
            companyReception: _detailsModel.result.reference),
        SizedBox(
          height: 10,
        ),
        InkWell(
          onTap: ()=>Navigator.push(context, CupertinoPageRoute(builder: (_)=>MenuPage())),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 45,
            decoration: BoxDecoration(
                color: MyColors.primary, borderRadius: BorderRadius.circular(15)),
            alignment: Alignment.center,
            child: MyText(
              title: "Next",
              color: MyColors.white,
              size: 16,
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        InkWell(
          onTap: () {
            setState(() {
              _details = !_details;
            });
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 45,
            decoration: BoxDecoration(
                color: MyColors.white,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                    color: Colors.blueGrey.withOpacity(.6), width: 2)),
            alignment: Alignment.center,
            child: MyText(
              title: "Search again",
              color: Colors.blueGrey.withOpacity(.6),
              size: 16,
            ),
          ),
        ),
      ],
    );
  }

  Widget searchResult() {
    return Column(
      children: [
        Row(
          children: [
            Icon(
              Icons.location_city,
              color: MyColors.grey,
            ),
            SizedBox(
              width: 10,
            ),
            MyText(
              title: tr("setYourLocation"),
              size: 16,
              color: MyColors.grey,
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        LabelTextField(
          isPassword: false,
          controller: _searchWork,
          label: tr("workAddress"),
          icon: Icon(
            Icons.search_outlined,
            color: MyColors.primary,
          ),
          onChange: (val) {
            if(val!=null)
            _getLocations();
          },
        ),
        SizedBox(height: 10,),
        _loading
            ? Center(child: CircularProgressIndicator())
            : Flexible(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: _locationsModel.predictions == null
                        ? 0
                        : _locationsModel.predictions.length,
                    itemBuilder: (_, index) => ListTile(
                          onTap: () => _getLocationDetails(
                              _locationsModel.predictions[index].placeId),
                          leading: Icon(
                            Icons.location_on,
                            color: MyColors.grey.withOpacity(.3),
                          ),
                          title: MyText(
                            title: _locationsModel
                                .predictions[0].structuredFormatting.mainText,
                            color: Colors.blueGrey.withOpacity(.6),
                            size: 17,
                            overflow: TextOverflow.ellipsis,
                          ),
                          subtitle: MyText(
                            title: _locationsModel.predictions[0]
                                .structuredFormatting.secondaryText,
                            color: MyColors.grey,
                            size: 14,
                          ),
                        )),
              )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      backgroundColor: MyColors.white,
      appBar: AppBar(
        backgroundColor: MyColors.white,
        leading: Icon(
          Icons.arrow_back,
          color: MyColors.primary,
        ),
        title: MyText(
          title: tr("signUp"),
          size: 20,
          color: MyColors.primary,
        ),
        centerTitle: true,
        elevation: 0,
        actions: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
            child: Center(
              child: MyText(
                title: tr("signIn"),
                size: 16,
                color: MyColors.white,
              ),
            ),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.yellow, Colors.yellow.shade900],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.6, 1.2),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
                borderRadius: BorderRadius.circular(10)),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: Container(
              height: 60,
              margin: const EdgeInsets.symmetric(horizontal: 10),
              child: TabBar(
                controller: _controller,
                physics: NeverScrollableScrollPhysics(),
                onTap: (index) {
                  setState(() {
                    _controller.index = 0;
                  });
                },
                labelColor: MyColors.primary,
                indicatorSize: TabBarIndicatorSize.tab,
                indicator: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 4, color: MyColors.primary))),
                unselectedLabelColor: Colors.grey,
                labelPadding: EdgeInsets.symmetric(vertical: 0),
                labelStyle: TextStyle(
                  fontSize: 16,
                  fontFamily: "ibmPlex",
                ),
                tabs: [
                  Tab(
                    text: "${tr("location")}",
                  ),
                  Tab(
                    text: "${tr("account")}",
                  ),
                  Tab(
                    text: "${tr("verify")}",
                  ),
                ],
              )),
        ),
      ),
      body:_loadingPage?Center(child: CircularProgressIndicator())
          :  GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: _details ? viewDetails() : searchResult(),
        ),
      ),
    );
  }
}
