import 'package:flutter/material.dart';
import 'package:menu_task/app/models/menuModel.dart';
import 'package:menu_task/app/repository/Repository.dart';
import 'package:menu_task/general/constants/MyColors.dart';
import 'package:menu_task/general/constants/MyText.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  Repository _repository;
  MenuModel _menuModel = MenuModel();
  @override
  void initState() {
    setState(() {
      _repository = Repository(_scaffold);
    });
    super.initState();
  }



  Widget _selectedCat({String title, bool selected}) {
    return InkWell(
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
          color: selected ? MyColors.primary : MyColors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        child: MyText(
          title: title,
          size: 14,
          color: selected ? MyColors.white : Colors.blueGrey.withOpacity(.6),
        ),
      ),
    );
  }

  Widget _itemBuild(
      {String img,
      String title,
      String beforePrice,
      String afterPrice,
      String restaurantName}) {
    return Container(
      width: 150,
      child: Column(
        children: [
          Card(
            elevation: 2,
            margin: const EdgeInsets.all(0),
            child: Container(
              width: 150,
              height: 120,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                      image: NetworkImage(img), fit: BoxFit.fill)),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          ),
          MyText(
            title: beforePrice,
            size: 13,
            color: Colors.blueGrey.withOpacity(.6),
            decoration: TextDecoration.lineThrough,
          ),
          Row(
            children: [
              Image.asset(
                "images/icn_plus.png",
                width: 40,
                height: 20,
                fit: BoxFit.fill,
              ),
              SizedBox(
                width: 5,
              ),
              MyText(
                title: "EGP $afterPrice",
                size: 14,
                color: Colors.black,
              ),
            ],
          ),
          Container(
            width: 150,
            child: MyText(
              title: title,
              size: 15,
              color: Colors.black,
              maxLines: 2,
            ),
          ),
          MyText(
            title: restaurantName,
            size: 12,
            color: Colors.blueGrey.withOpacity(.6),
            maxLines: 1,
          ),
        ],
      ),
    );
  }

  Widget _listing({String img, List<BestValue> products}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            "$img",
            width: 80,
            height: 80,
          ),
          Container(
            height: 220,
            child: ListView.builder(
                itemCount: products.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (_, index) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: _itemBuild(
                          img: products[index].img,
                          title: products[index].name,
                          afterPrice: products[index].regularPrice.toString(),
                          beforePrice: products[index].plusPrice.toString(),
                          restaurantName:
                              products[index].restaurant.toString()),
                    )),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80),
        child: Container(
          height: 80,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          alignment: Alignment.bottomCenter,
          child: Row(
            children: [
              Image.asset(
                "images/icn_delivery.png",
                width: 25,
                height: 25,
                fit: BoxFit.fill,
              ),
              SizedBox(width: 10),
              MyText(
                title: "Delivery to ",
                size: 16,
                color: Colors.black,
              ),
              MyText(
                title: "Stc ",
                size: 16,
                fontWeight: FontWeight.w900,
                color: Colors.black,
              ),
              Icon(Icons.keyboard_arrow_down)
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 30,
            ),
            Image.asset(
              "images/Meal_on_the_go.png",
              width: 120,
              height: 80,
            ),
            SizedBox(
              height: 10,
            ),
            Image.asset(
              "images/banner.png",
              width: MediaQuery.of(context).size.width,
              height: 100,
              fit: BoxFit.fill,
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title: "Order By 11:30 AM ",
                      size: 14,
                      fontWeight: FontWeight.w900,
                      color: MyColors.primary,
                    ),
                    MyText(
                      title: "Select Your Meal ",
                      size: 12,
                      color: Colors.blueGrey.withOpacity(.6),
                    ),
                  ],
                ),
                Spacer(),
                _selectedCat(title: "All", selected: true),
                _selectedCat(title: "Syami", selected: false),
                _selectedCat(title: "Healthy", selected: false),
              ],
            ),
            FutureBuilder<MenuModel>(
                future:_repository.getMenu(),
                builder: (context,snapShot){
              if(snapShot.hasData){
                return Column(
                  children: [
                    _listing(
                        img: "images/title_light.png",
                        products: snapShot.data.light),
                    SizedBox(
                      height: 10,
                    ),
                    _listing(
                        img: "images/title_best_value.png",
                        products: snapShot.data.bestValue),
                    SizedBox(
                      height: 10,
                    ),
                    _listing(
                        img: "images/title_plus.png",
                        products: snapShot.data.plus),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                );
              }else{
                return Container(
                  height: MediaQuery.of(context).size.height * .4,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            })

          ],
        ),
      ),
    );
  }
}
