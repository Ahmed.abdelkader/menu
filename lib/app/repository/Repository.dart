import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:menu_task/app/models/locationDetailsModel.dart';
import 'package:menu_task/app/models/locationsModel.dart';
import 'package:menu_task/app/models/menuModel.dart';
import 'package:menu_task/app/repository/LocationHttpMethods.dart';
import 'package:menu_task/app/repository/menuHttpMethods.dart';

class Repository {
  GlobalKey<ScaffoldState> scaffold;
  LocationHttpMethods _locationHttpMethods;
  MenuHttpMethods _menuHttpMethods;
  Repository(this.scaffold) {
    _locationHttpMethods = LocationHttpMethods(scaffold);
    _menuHttpMethods=MenuHttpMethods(scaffold);
  }

  Future<LocationsModel> getLocations(String val) async =>
      _locationHttpMethods.getLocations(val);
  Future<LocationDetailsModel> getLocationDetails(String placeId) async =>
      _locationHttpMethods.getLocationDetails(placeId);
  Future<MenuModel> getMenu() async=>_menuHttpMethods.getMenu();
}
