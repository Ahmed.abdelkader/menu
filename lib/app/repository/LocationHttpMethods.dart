import 'package:flutter/material.dart';
import 'package:menu_task/general/constants/Http.dart';
import 'package:menu_task/app/models/locationDetailsModel.dart';
import 'package:menu_task/app/models/locationsModel.dart';

class LocationHttpMethods {
  GlobalKey<ScaffoldState> scaffold;
  LocationHttpMethods(this.scaffold);

  Future<LocationsModel> getLocations(String val) async {
    Map<String, String> body = {};
    String _url =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyB-PBPHzbYC90YVPMuEaM7cCMpEhayuZ4I&input=$val&language=en&components=country%3Aeg&types=establishment";
    print("url $_url");
    var _data = await Http(scaffold).post(
      _url,
      body,
    );
    if (_data != null) {
      return LocationsModel.fromJson(_data);
    } else {
      return LocationsModel();
    }
  }
  Future<LocationDetailsModel> getLocationDetails(String placeId) async {
    Map<String, String> body = {};
    String _url =
        "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyB-PBPHzbYC90YVPMuEaM7cCMpEhayuZ4I&place_id=$placeId&language=en";
    var _data = await Http(scaffold).post(
      _url,
      body,
    );
    if (_data != null) {
      return LocationDetailsModel.fromJson(_data);
    } else {
      return LocationDetailsModel();
    }
  }
}
