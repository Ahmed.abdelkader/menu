import 'package:flutter/material.dart';
import 'package:menu_task/app/models/menuModel.dart';
import 'package:menu_task/general/constants/Http.dart';

class MenuHttpMethods{
  GlobalKey<ScaffoldState> scaffold;
  MenuHttpMethods(this.scaffold);

  Future<MenuModel> getMenu() async {
    String _url =
        "https://demo4833373.mockable.io/menu";
    print("url $_url");
    var _data = await Http(scaffold).get(
      _url,{}
    );
    print("object $_data");
    if (_data != null) {
      return MenuModel.fromJson(_data);
    } else {
      return MenuModel();
    }
  }

}