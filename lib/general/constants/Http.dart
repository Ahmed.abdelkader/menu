import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'package:http_parser/http_parser.dart';
import 'package:menu_task/general/constants/GlobalState.dart';


class Http{


  GlobalKey<ScaffoldState> _scaffold;

  Http(this._scaffold);



  get(String url,Map<String,String>  body)async{
    var header={
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GlobalState.instance.get("token")}',
    };
    print("statusCode $header");
    BuildContext context=_scaffold.currentContext;

    var response = await http.get("$url",headers: header);
    print("statusCode ${response.statusCode}");
    if(response.statusCode==200){
      var data=json.decode(response.body);
      print(data);
      return data;

    }

    return null;

  }

  post(url,body,{close=false,token})async{

    var header={
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GlobalState.instance.get("token")}',
    };
    print(body);
    var response = await http.post("$url",body: body,headers: header);
    print("response ${response.body}");
    print(response.statusCode);
    if(close){
      Navigator.of(_scaffold.currentContext).pop();
    }
    if(response.statusCode==200){
      var data=json.decode(response.body);
      print(data);
        return data;
    }
    return null;
  }
  uploadFile(String url,Map<String,dynamic> body,{close = false,token}) async{
    var header={
      'Accept': 'application/json',
      'Authorization': 'Bearer ${GlobalState.instance.get("token")}',
    };
    print(body);
    BuildContext context=_scaffold.currentContext;
    //create multipart request for POST or PATCH method
    var request = http.MultipartRequest("POST", Uri.parse("$url"));
    request.headers.addAll(header);
    //add text fields
    body.forEach((key, value) async {
      if((value) is File){
        //create multipart using filepath, string or bytes
        var pic = await http.MultipartFile.fromPath("$key", value.path,contentType: MediaType('image', 'jpg'));
        //add multipart to request
        request.files.add(pic);
      }else if((value) is List<File>){
        value.forEach((element) async {
          var pic = await http.MultipartFile.fromPath("$key", element.path,contentType: MediaType('image', 'jpg'));
          //add multipart to request
          request.files.add(pic);
        });
      }else{
        request.fields["$key"] = "$value";
      }
    });


    var response = await request.send();

    if (close) {
      Navigator.of(context).pop();
    }

    //Get the response from the server
    var responseData = await response.stream.toBytes();
    var responseString = utf8.decode(responseData);
    print(responseString);
    if(response.statusCode==200){
      var data=json.decode(responseString);
      if(data["key"]==1){
        return data;
      }
    }
    return null;
  }


}