import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MyText extends StatelessWidget {
  String title;
  Color color = Colors.black;
  double size = 16;
  TextAlign alien = TextAlign.start;
  TextDecoration decoration= TextDecoration.none;
  TextOverflow overflow;
  FontWeight fontWeight;
  String fontFamily;
  int maxLines;

  MyText({this.title, this.color, this.size, this.alien,this.decoration,this.overflow,this.fontWeight,this.fontFamily,this.maxLines});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Text(
      "$title",
      textAlign: alien,
      textScaleFactor: 1.0,
      style: TextStyle(color: color, fontSize: size, fontFamily:fontFamily?? "ibmPlex",decoration: decoration,fontWeight: fontWeight),
      overflow: overflow,
      maxLines: maxLines,
    );
  }
}
