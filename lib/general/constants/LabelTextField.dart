import 'package:flutter/material.dart';
import 'package:menu_task/general/constants/MyColors.dart';

// ignore: must_be_immutable
class LabelTextField extends StatelessWidget{

  TextEditingController controller;
  String label;
  EdgeInsets margin=EdgeInsets.all(0);
  bool isPassword=false;
  TextInputType type=TextInputType.text;
  Function onChange;
  Widget icon;

  LabelTextField({this.label,this.controller,this.margin,this.isPassword,this.type,this.onChange,this.icon});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: margin,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),border: Border.all(color: MyColors.primary,width: 2)),
      child: TextFormField(
        onChanged: onChange,
        controller: controller,
        keyboardType: type,
        obscureText: isPassword,
        style: TextStyle(fontSize: 14,fontFamily: "cairo",color: Colors.black.withOpacity(.7)),
        decoration: InputDecoration(
          labelText: "  ${label??""}  ",
          labelStyle: TextStyle(fontFamily: "ibmPlex",fontSize: 14),
          contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
          suffixIcon: icon
        ),
      ),
    );
  }


}