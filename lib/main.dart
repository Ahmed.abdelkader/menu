


import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:menu_task/app/screens/chooseLocation.dart';

void main(){
  runApp(EasyLocalization(
    child: MyApp(),
    supportedLocales: [Locale('ar', 'EG'),Locale('en', 'US')],
    path: 'assets/langs',
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  State<StatefulWidget> createState() {
    return _myAppState();
  }
}

class _myAppState extends State<MyApp>{

  static final navigatorKey = new GlobalKey<NavigatorState>();

  var _local= Locale('ar', 'EG');


  _setChangeLanguage(local){
    _local= local;
    context.locale=_local;
    setState(() {

    });
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      title: 'Menu task',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        cursorColor: Colors.grey,
        focusColor: Colors.grey,
        accentColor: Colors.grey,
        primaryColor: Colors.grey,
        platform: TargetPlatform.android,
        pageTransitionsTheme: PageTransitionsTheme(builders: {TargetPlatform.android: CupertinoPageTransitionsBuilder(),}),
      ),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: Locale('en', 'US'),
      home: ChooseLocation(),

    );
  }

}